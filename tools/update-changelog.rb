#!/usr/bin/env ruby

require "uri"
require "net/http"
require "openssl"
require "json"
require "optparse"

GitlabError = Class.new(StandardError)
GitlabClientError = Class.new(GitlabError)

def gitlab_client(path, method, body = nil)
  url = URI("https://gitlab.com#{path}")

  http = Net::HTTP.new(url.host, url.port)
  http.use_ssl = true

  request = case method
            when :get
              Net::HTTP::Get.new(url)
            when :post
              Net::HTTP::Post.new(url)
            when :put
              Net::HTTP::Put.new(url)
            else
              raise "Unknown method: #{method}"
            end

  request["Content-Type"] = "application/json"
  request["Private-Token"] = ENV["GITLAB_TOKEN"]
  request.body = JSON.dump(body) if body

  response = http.request(request)

  case response
  when Net::HTTPSuccess
    JSON.parse(response.read_body)
  when Net::HTTPClientError
    raise GitlabClientError, "HTTP #{response.code} for #{method} #{url}: #{response.read_body}"
  else
    raise GitLabError, "HTTP #{response.code} for #{method} #{url}: #{response.read_body}"
  end
end

def list_commits_for_tag(tag)
  commits = `git rev-list --no-merges $(git describe --tags --abbrev=0 #{tag}^)..#{tag}`.lines
  commits.map(&:chomp)
end

def get_mrs_for_commit(commit_id)
  gitlab_client("/api/v4/projects/gitlab-org%2ffluent-plugin-redis-slowlog/repository/commits/#{commit_id}/merge_requests", :get)
end

def create_release_for_tag(tag, description)
  req_body = { tag_name: tag, description: description }
  gitlab_client("/api/v4/projects/gitlab-org%2ffluent-plugin-redis-slowlog/releases", :post, req_body)
end

def update_release_for_tag(tag, description)
  req_body = { description: description }
  gitlab_client("/api/v4/projects/gitlab-org%2ffluent-plugin-redis-slowlog/releases/#{tag}", :put, req_body)
end

def get_unique_mrs_for_tag(tag)
  commit_ids = list_commits_for_tag(tag)
  dup_mrs = commit_ids.map { |commit_id| get_mrs_for_commit(commit_id) }.flatten
  uniq_mrs = dup_mrs.uniq { |mr| mr["iid"] }

  uniq_mrs.sort { |mr| mr["iid"] }.reverse
end

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: update-changelog.rb [options] tag"

  opts.on("-d", "--[no-]dry-run", "Display only. Do not update release notes.") do |d|
    options[:dry_run] = d
  end
end.parse!

# Check required conditions
unless ARGV.length == 1
  puts optparse
  exit(-1)
end

tag = ARGV.first

changelog = get_unique_mrs_for_tag(tag).map { |mr| "* #{mr['web_url']}: #{mr['title']}" }.join("\n")

raise "No new changes found" if changelog.empty?

changelog = "## Changes in #{tag}\n\n" + changelog

if options[:dry_run]
  puts changelog
  exit
end

begin
  create_release_for_tag(tag, changelog)
  puts "Created release: https://gitlab.com/gitlab-org/fluent-plugin-redis-slowlog/-/releases##{tag}"
rescue GitlabClientError
  update_release_for_tag(tag, changelog)
  puts "Updated release: https://gitlab.com/gitlab-org/fluent-plugin-redis-slowlog/-/releases##{tag}"
end
